<?php
/*
 * Theme.php
 * =============================================================================
 */

//namespace
namespace Shopware\Themes\Graceandholmes;

//usages
use Doctrine\Common\Collections\ArrayCollection;
use Shopware\Components\Form as Form;
use Shopware\Components\Theme\ConfigSet;

//class
class Theme extends \Shopware\Components\Theme {
    
    protected $extend       = 'Bare';

    protected $name         = 'Grace & Holmes';

    protected $description  = 'Theme Beschreibung';

    protected $author       = 'jiam';

    protected $license      = 'MIT';
    
    /**
     * JavaScript
     * -------------------------------------------------------------------------
     * @var     {ærray}         $javascript
     */
    protected $javascript   = array(
        'vendors/js/jquery/jquery.min.js',
        
        'vendors/js/picturefill/picturefill.js',
        
        'vendors/js/jquery.transit/jquery.transit.js',
        
        'vendors/js/jquery.event.move/jquery.event.move.js',
        'vendors/js/jquery.event.swipe/jquery.event.swipe.js',
        
        'vendors/js/masonry/masonry.pkgd.min.js',
        
        'src/js/jquery.ie-fixes.js',
        'src/js/jquery.plugin-base.js',
        'src/js/jquery.state-manager.js',
        'src/js/jquery.storage-manager.js',
        'src/js/jquery.off-canvas-menu.js',
        'src/js/jquery.search.js',
        'src/js/jquery.tab-menu.js',
        'src/js/jquery.image-slider.js',
        'src/js/jquery.image-zoom.js',
        'src/js/jquery.collapse-panel.js',
        'src/js/jquery.auto-submit.js',
        'src/js/jquery.scroll.js',
        'src/js/jquery.product-slider.js',
        'src/js/jquery.register.js',
        'src/js/jquery.modal.js',
        'src/js/jquery.selectbox-replacement.js',
        'src/js/jquery.captcha.js',
        'src/js/jquery.drop-down-menu.js',
        'src/js/jquery.loading-indicator.js',
        'src/js/jquery.overlay.js',
        'src/js/jquery.form-polyfill.js',
        'src/js/jquery.pseudo-text.js',
        'src/js/jquery.last-seen-products.js',
        'src/js/jquery.lightbox.js',
        'src/js/jquery.ajax-product-navigation.js',
        'src/js/jquery.newsletter.js',
        'src/js/jquery.menu-scroller.js',
        'src/js/jquery.shipping-payment.js',
        'src/js/jquery.add-article.js',
        'src/js/jquery.range-slider.js',
        'src/js/jquery.filter-component.js',
        'src/js/jquery.listing-actions.js',
        'src/js/jquery.collapse-cart.js',
        'src/js/jquery.emotion.js',
        'src/js/jquery.product-compare-add.js',
        'src/js/jquery.product-compare-menu.js',
        'src/js/jquery.infinite-scrolling.js',
        'src/js/jquery.off-canvas-button.js',
        'src/js/jquery.subcategory-nav.js',
        'src/js/jquery.ajax-wishlist.js',
        'src/js/jquery.preloader-button.js',
        'src/js/jquery.image-gallery.js',
        'src/js/jquery.offcanvas-html-panel.js',
        'src/js/jquery.jump-to-tab.js',
        'src/js/jquery.ajax-variant.js',
        'src/js/jquery.shopware-responsive.js',
        
        // Advanced Menu
        'src/js/jquery.advanced-menu.js',
        
        // Grace and Holmes Theme Script
        'src/js/gah.theme.js'
    );
    
    /**
     * Field Set Defaults
     * -------------------------------------------------------------------------
     * @var     {array}         $fieldSetDefaults
     */
    private $fieldSetDefaults   = array(
        'layout'    => 'column',
        'height'    => 170,
        'flex'      => 0,
        'defaults'  => array(
            'columnWidth'   => 0.5, 
            'labelWidth'    => 180, 
            'margin'        => '3 16 3 0'
        )
    );

    /**
     * Theme Color Defaults
     * -------------------------------------------------------------------------
     * @var     {ærray}         $themeColorDefaults
     */
    private $themeColorDefaults = array();
    
    /**
     * Create Config
     * -------------------------------------------------------------------------
     * @access      public
     * @uses        $this->createMainConfigTab
     *              $this->createBottomTabPanel
     */
    public function createConfig(Form\Container\TabContainer $container)
    {
        $container->addTab($this->createMainConfigTab());

        // Social Network Tab
        $socialNetworksTab = $this->createTab(
            'socialnetworks_tab',
            'Soziale Netzwerke'
        );
        $container->addTab($socialNetworksTab);
        
        $socialNetworksTab->addElement($this->createSocialNetworksTab());
    }
    
    
    /**
     * Create Main Navigation Tab
     * -------------------------------------------------------------------------
     * @access      private
     * @return      $fieldSet
     */
    private function createMainNavigationTab(){
        $attributes = array_merge($this->fieldSetDefaults, ['height' => 200]);
    }
    
    /**
     * Create Social Networks Tab
     * -------------------------------------------------------------------------
     * @access      private
     * @return      $fieldSet
     */
    private function createSocialNetworksTab(){
        $attributes = array_merge($this->fieldSetDefaults, ['height' => 200]);
        
        $fieldSet = $this->createFieldSet(
            'social_networks_field_set',
            'Externe Verweise von Sozialen Netwerken',
            ['attributes' => $attributes]
        );
        
        // Facebook
        $fieldSet->addElement(
            $this->createTextField(
                'facebookUrl',
                'Facebook',
                '',
                ['attributes' =>
                    ['lessCompatible'   => false], 
                 'help'                 => 'http://facebook.com/ID'
                ]
            )
        );
        // Google+
        $fieldSet->addElement(
            $this->createTextField(
                'googleplusUrl',
                'Google+',
                '',
                ['attributes' =>
                    ['lessCompatible'   => false], 
                 'help'                 => 'http://google.com/ID'
                ]
            )
        );
        // Instagram
        $fieldSet->addElement(
            $this->createTextField(
                'instagramUrl',
                'Instagram',
                '',
                ['attributes' =>
                    ['lessCompatible'   => false], 
                 'help'                 => 'http://instagram.com/ID'
                ]
            )
        );
        // Pinterest
        $fieldSet->addElement(
            $this->createTextField(
                'pinterestUrl',
                'Pinterest',
                '',
                ['attributes' =>
                    ['lessCompatible'   => false], 
                 'help'                 => 'http://pinterest.com/ID'
                ]
            )
        );
        // Twitter
        $fieldSet->addElement(
            $this->createTextField(
                'twitterUrl',
                'Twitter',
                '',
                ['attributes' =>
                    ['lessCompatible'   => false], 
                 'help'                 => 'http://twitter.com/ID'
                ]
            )
        );
        // Youtube
        $fieldSet->addElement(
            $this->createTextField(
                'youtubeUrl',
                'Youtube',
                '',
                ['attributes' =>
                    ['lessCompatible'   => false], 
                 'help'                 => 'http://youtube.com/ID'
                ]
            )
        );
        
        return $fieldSet;
    }

    /**
     * Create Main Config Tab
     * Helper function to create the main tab ("Responsive configuration")
     * -------------------------------------------------------------------------
     * @return      {array}     $tab
     */
    private function createMainConfigTab()
    {
        // New Tab
        // -------
        
        // Responsive Tab Header
        $tab = $this->createTab(
            'responsiveMain',
            '__responsive_tab_header__',
            [
                'attributes' => [
                    'layout' => 'anchor',
                    'autoScroll' => true,
                    'padding' => '0',
                    'defaults' => ['anchor' => '100%']
                ]
            ]
        );
        
        // New Field Set
        // -------------

        // Global Configuration
        $fieldSet = $this->createFieldSet(
            'bareGlobal',
            '__global_configuration__',
            [
                'attributes' => [
                    'padding' => '10',
                    'margin' => '5',
                    'layout' => 'anchor',
                    'defaults' => ['labelWidth' => 155, 'anchor' => '100%']
                ]
            ]
        );
            // Offcanvas Cart
            $fieldSet->addElement(
                $this->createCheckboxField(
                    'offcanvasCart',
                    '__offcanvas_cart__',
                    true,
                    $this->getLabelAttribute(
                        'offcanvas_cart_description'
                    )
                )
            );
            // Offcanvas Move Method
            $fieldSet->addElement(
                $this->createCheckboxField(
                    'offcanvasOverlayPage',
                    '__offcanvas_move_method__',
                    true,
                    $this->getLabelAttribute(
                        'offcanvas_move_method_description'
                    )
                )
            );
            // Focus Search
            $fieldSet->addElement(
                $this->createCheckboxField(
                    'focusSearch',
                    '__focus_search__',
                    false,
                    $this->getLabelAttribute(
                        'focus_search_description'
                    )
                )
            );
            // Display Sidebar
            $fieldSet->addElement(
                $this->createCheckboxField(
                    'displaySidebar',
                    '__display_sidebar__',
                    true,
                    $this->getLabelAttribute(
                        'display_sidebar_description'
                    )
                )
            );
            // Checkout Header
            $fieldSet->addElement(
                $this->createCheckboxField(
                    'checkoutHeader',
                    '__checkout_header__',
                    true,
                    $this->getLabelAttribute(
                        'checkout_header_description'
                    )
                )
            );
            // Checkout Footer
            $fieldSet->addElement(
                $this->createCheckboxField(
                    'checkoutFooter',
                    '__checkout_footer__',
                    true,
                    $this->getLabelAttribute(
                        'checkout_footer_description'
                    )
                )
            );
            // Infinite Scrolling
            $fieldSet->addElement(
                $this->createCheckboxField(
                    'infiniteScrolling',
                    '__enable_infinite_scrolling__',
                    true,
                    $this->getLabelAttribute(
                        'enable_infinite_scrolling_description'
                    )
                )
            );
            // Infinite Threshold
            $fieldSet->addElement(
                $this->createNumberField(
                    'infiniteThreshold',
                    '__infinite_threshold__',
                    4,
                    $this->getLabelAttribute(
                        'infinite_threshold_description',
                        'supportText'
                    )
                )
            );
            // Lightbox Zoom Factor
            $fieldSet->addElement(
                $this->createSelectField(
                    'lightboxZoomFactor',
                    '__lightbox_zoom_factor__',
                    0,
                    [
                        ['value' => 0, 'text' => '__lightbox_zoom_factor_auto__'],
                        ['value' => 1, 'text' => '__lightbox_zoom_factor_none__'],
                        ['value' => 2, 'text' => '__lightbox_zoom_factor_2x__'],
                        ['value' => 3, 'text' => '__lightbox_zoom_factor_3x__'],
                        ['value' => 5, 'text' => '__lightbox_zoom_factor_5x__']
                    ],
                    $this->getLabelAttribute(
                        'lightbox_zoom_factor_description',
                        'supportText'
                    )
                )
            );
            // Apple Wep App Title
            $fieldSet->addElement(
                $this->createTextField(
                    'appleWebAppTitle',
                    '__apple_web_app_title__',
                    '',
                    [
                        'attributes' => ['lessCompatible' => false]
                    ]
                )
            );
            // Ajax Variant
            $fieldSet->addElement(
                $this->createCheckboxField(
                    'ajaxVariantSwitch',
                    '__ajax_variant_switch__',
                    true,
                    [
                        'attributes' => [
                            'lessCompatible' => false,
                            'boxLabel' => Shopware()->Snippets()->getNamespace('themes/bare/backend/config')->get('ajax_variant_switch_description')
                        ]
                    ]
                )
            );

        // Add Field Set
        $tab->addElement($fieldSet);

        // New Field Set
        // -------------
        
        // Advanced Settings
        $fieldSet = $this->createFieldSet(
            'responsiveGlobal',
            '__advanced_settings__',
            [
                'attributes' => [
                    'padding' => '10',
                    'margin' => '5',
                    'layout' => 'anchor',
                    'defaults' => ['anchor' => '100%', 'labelWidth' => 155]
                ]
            ]
        );
        
            // Additional CSS
            $fieldSet->addElement(
                $this->createTextAreaField(
                    'additionalCssData',
                    '__additional_css_data__',
                    '',
                    ['attributes' => 
                        [   'xtype' => 'textarea', 
                            'lessCompatible' => false   ], 
                        'help' => '__additional_css_data_description__'
                    ]
                )
            );
            // Additional JS
            $fieldSet->addElement(
                $this->createTextAreaField(
                    'additionalJsLibraries',
                    '__additional_js_libraries__',
                    '',
                    ['attributes' => 
                        [   'xtype' => 'textarea', 
                            'lessCompatible' => false   ], 
                        'help' => '__additional_js_libraries_description__'
                    ]
                )
            );
            
        // Add Field Set
        $tab->addElement($fieldSet);
        
        return $tab;
    }

    /**
     * Get Label Attribute
     * Helper function to get the attribute of a checkbox field which shows a description label
     * -------------------------------------------------------------------------
     * @param                   $snippetName
     * @return      {array}
     */
    private function getLabelAttribute($snippetName, $labelType = 'boxLabel')
    {
        $description = Shopware()->Snippets()->getNamespace('themes/bare/backend/config')->get($snippetName);
        
        return [
            'attributes' => [$labelType => $description]
        ];
    }

    /**
     * Create Config Sets
     * Helper function to merge default theme colors with color schemes
     * -------------------------------------------------------------------------
     * @param       {array}     $collection
     */
    public function createConfigSets(ArrayCollection $collection)
    {
        //
    }
}