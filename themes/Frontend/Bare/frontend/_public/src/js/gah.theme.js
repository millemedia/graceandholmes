(function($){
    var gah_navFlag,
        gah_curPos,
        gah_tempPos;


    $(window).bind("scroll", function(event){
        gah_curPos = $("html").scrollTop() || $("body").scrollTop();

        var gah_topBarHeight    = $(".header-main").height();

        if(gah_curPos >= gah_topBarHeight && gah_navFlag !== "fixed"){
            $(".navigation-main--wrapper")
                .addClass("is--fixed");

            gah_navFlag = "fixed";
        }
        else if(gah_curPos < gah_topBarHeight){
            $(".navigation-main--wrapper")
                .removeClass("is--fixed");

            gah_navFlag = "";
        }

        gah_tempPos = gah_curPos;
    });
    
    /*
    $(".tinymce-editor-image").each(function(index){
        $(this).html(function(index, text){
            return text.replace("<a href=\"" + this.src + "\">" + text + "</a>");
        });
    });
    */
   
})(jQuery);