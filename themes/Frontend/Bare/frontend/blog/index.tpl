{extends file='frontend/index/index.tpl'}

{block name='frontend_index_header'}
    {include file='frontend/blog/header.tpl'}
{/block}

{block name='frontend_index_body'}
        <body class="is--ctl-{controllerName} is--act-{controllerAction}{if $sCategoryContent.id == 84 || $sCategoryContent.parentId == 84} is--ctl-portfolio{/if}{if $sUserLoggedIn} is--user{/if}{if $sTarget} is--target-{$sTarget}{/if}{if $theme.checkoutHeader && ( ({controllerName} == "checkout" && {controllerAction} != "cart") || ({controllerName} == "register" && $sTarget != "account") ) } is--minimal-header{/if}{if !$theme.displaySidebar} is--no-sidebar{/if}">
{/block}

{block name="frontend_index_content_top" append}
    {* Category headline *}
    {if $sCategoryContent.cmsheadline || $sCategoryContent.cmstext}
        {include file="frontend/listing/text.tpl"}
    {/if}
{/block}

{* Main content *}
{block name='frontend_index_content'}
	<div class="blog--content block-group">
            {* Blog Sidebar *}
            {block name='frontend_blog_listing_sidebar'}
                    {include file='frontend/blog/listing_sidebar.tpl'}
            {/block}

            {* Blog Banner *}
            {block name='frontend_blog_index_banner'}
                    {include file="frontend/listing/banner.tpl"}
            {/block}

            {* Blog listing *}
            {block name='frontend_blog_index_listing'}
                    {include file="frontend/blog/listing.tpl"}
            {/block}
	</div>
{/block}