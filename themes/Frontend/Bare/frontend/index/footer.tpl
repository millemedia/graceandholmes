{* Footer menu *}
{block name='frontend_index_footer_menu'}
    
    <div class="footer--top">
        <div class="container">
            
            <div class="footer--columns block-group">
                {include file='frontend/index/footer-navigation.tpl'}
            </div>
            
        </div>
    </div>
    
{/block}

{* Social Networks in the Footer*}
{block name='frontend_index_footer_socialnetworks'}
    
    <div class="footer--social-networks">
        <div class="container">
            <ul class="social-networks--list">
                {if $theme.facebookUrl !== ''}
                    <li class="social-networks--facebook">
                        <a href="{$theme.facebookUrl}" target="_blank">
                            <span>Facebook</span>
                        </a>
                    </li>
                {/if}
                {if $theme.googleplusUrl !== ''}
                    <li class="social-networks--googleplus">
                        <a href="{$theme.googleplusUrl}" target="_blank">
                            <span>Google+</span>
                        </a>
                    </li>
                {/if}
                {if $theme.instagramUrl !== ''}
                    <li class="social-networks--instagram">
                        <a href="{$theme.instagramUrl}" target="_blank">
                            <span>Instagram</span>
                        </a>
                    </li>
                {/if}
                {if $theme.pinterestUrl !== ''}
                    <li class="social-networks--pinterest">
                        <a href="{$theme.pinterestUrl}" target="_blank">
                            <span>Pinterest</span>
                        </a>
                    </li>
                {/if}
                {if $theme.twitterUrl !== ''}
                    <li class="social-networks--twitter">
                        <a href="{$theme.twitterUrl}" target="_blank">
                            <span>Twitter</span>
                        </a>
                    </li>
                {/if}
                {if $theme.youtubeUrl !== ''}
                    <li class="social-networks--youtube">
                        <a href="{$theme.youtubeUrl}" target="_blank">
                            <span>Youtube</span>
                        </a>
                    </li>
                {/if}
            </ul>
        </div>
    </div>
    
{/block}

{* Copyright in the footer *}
{block name='frontend_index_footer_copyright'}

    <div class="footer--bottom">
        <div class="container">

            <div class="footer--partner">
                <ul class="footer--partner-list">
                    <li>
                        <a class="fhs"></a>
                    </li>
                    <li>
                        <a class="young-guns"></a>
                    </li>
                    <li>
                        <a class="sos-partner" href="https://www.starofservice.de/pro/profilpublic/8515720" target="_blank"></a>
                    </li>   
                    <li>
                        <a class="houz" href="https://www.houzz.de/pro/bdemsa/grace-und-holmes" target="_blank"></a>
                    </li>   
                </ul>
            </div>
            
            {* Vat info *}
            {block name='frontend_index_footer_vatinfo'}
                <div class="footer--vat-info">
                    <p class="vat-info--text">
                        {if $sOutputNet}
                            {s name='FooterInfoExcludeVat' namespace="frontend/index/footer"}{/s}
                        {else}
                            {s name='FooterInfoIncludeVat' namespace="frontend/index/footer"}{/s}
                        {/if}
                    </p>
                </div>
            {/block}

        </div>
    </div>
      
{/block}
