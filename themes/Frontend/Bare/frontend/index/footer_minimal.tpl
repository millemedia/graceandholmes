{block name="frontend_index_minimal_footer"}
    <div class="container footer-minimal">

        {* Service menu *}
        {block name="frontend_index_minimal_footer_menu"}
            
            <div class="footer--top">
                <div class="container">
                
                    <div class="footer--service-menu">
                        {action module=widgets controller=index action=menu group=gLeft}
                    </div>
                    
                </div>
            </div>
            
        {/block}
        
        <div class="footer--bottom">
            <div class="container">

                    {* Vat info *}
                    {block name='frontend_index_minimal_footer_vat_info'}
                        <div class="footer--vat-info">
                            <p class="vat-info--text">
                                {if $sOutputNet}
                                        {s name='FooterInfoExcludeVat' namespace="frontend/index/footer"}{/s}
                                {else}
                                        {s name='FooterInfoIncludeVat' namespace="frontend/index/footer"}{/s}
                                {/if}
                            </p>
                        </div>
                    {/block}
                    
                </div>
            </div>
                
	</div>
{/block}