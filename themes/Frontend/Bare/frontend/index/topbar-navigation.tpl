<div class="top-bar">
	<div class="container block-group">

		{* Top bar navigation *}
		{block name="frontend_index_top_bar_nav"}
			<nav class="top-bar--navigation block" role="menubar">

				{action module=widgets controller=index action=shopMenu}

				{* Article Compare *}
				{block name='frontend_index_navigation_inline'}
					{if {config name="compareShow"}}
						<div class="navigation--entry entry--compare is--hidden" role="menuitem" aria-haspopup="true" data-drop-down-menu="true">
							{block name='frontend_index_navigation_compare'}
								{action module=widgets controller=compare}
							{/block}
						</div>
					{/if}
				{/block}

				{* Service / Support drop down *}
				{block name="frontend_index_checkout_actions_service_menu"}
					<div class="navigation--entry entry--service has--drop-down" role="menuitem" aria-haspopup="true" data-drop-down-menu="true">
						<i class="icon--service"></i> {s namespace='frontend/index/checkout_actions' name='IndexLinkService'}{/s}

						{* Include of the widget *}
						{block name="frontend_index_checkout_actions_service_menu_include"}
							{action module=widgets controller=index action=menu group=gLeft}
						{/block}
					</div>
				{/block}
				
					<div class="social-top" ><a href="https://www.facebook.com/graceandholmes/"></a></div>
					<div class="social-top" ><a href="https://www.instagram.com/bdemsa_graceandholmes/"></a></div>	
					<div class="social-top" ><a href="https://www.pinterest.co.uk/bdemsa/?autologin=true"></a></div>	
			</nav>
		{/block}

            {* Search form *}
            {block name='frontend_index_search'}
                <div class="top-bar--search block" role="menuitem" data-search="true" aria-haspopup="true"{if $theme.focusSearch && {controllerName} == 'index'} data-activeOnStart="true"{/if}>
                    <a class="btn entry--link entry--trigger" href="#show-hide--search" title="{"{s namespace='frontend/index/search' name="IndexTitleSearchToggle"}{/s}"|escape}">
                        <i class="icon--search"></i>

                        {block name='frontend_index_search_display'}
                            <span class="search--display">{s namespace='frontend/index/search' name="IndexSearchFieldSubmit"}{/s}</span>
                        {/block}
                    </a>

                    {* Include of the search form *}
                    {block name='frontend_index_search_include'}
                        {include file="frontend/index/search.tpl"}
                    {/block}
                </div>
            {/block}
	</div>
</div>