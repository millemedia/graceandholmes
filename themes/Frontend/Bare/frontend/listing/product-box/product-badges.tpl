{namespace name="frontend/listing/box_article"}

{* Small product badges on the left *}
{block name="frontend_listing_box_article_badges"}
    <div class="product--badges">
        
        {* Highlight badge *}
        {block name='frontend_listing_box_article_hint'}
            {if $sArticle.highlight}
                <div class="product--badge badge--recommend">
                    {s name="ListingBoxTip"}{/s}
                </div>
            {/if}
        {/block}
        
        {* Newcomer badge *}
        {block name='frontend_listing_box_article_new'}
            {if $sArticle.newArticle}
                <div class="product--badge badge--newcomer">
                    {s name="ListingBoxNew"}{/s}
                </div>
            {/if}
        {/block}
        
        {* Sold badge *}
        {block name='frontend_listing_box_article_sold'}
            {if !$sArticle.isAvailable}
                <div class="product--badge badge--sold">
                     Sold
                </div>
            {/if}
        {/block}

        {* Upcoming Badge *}
        {block name='frontend_listing_box_article_upcoming'}
            {if $sArticle.sUpcoming}
                <div class="product--badge badge--comming-soon">
                    Upcoming
                </div>
            {/if}
        {/block}

        {* Discount badge *}
        {block name='frontend_listing_box_article_discount'}
                {if $sArticle.has_pseudoprice}
                        <div class="product--badge badge--discount">
            <i class="icon--percent2"></i>
        </div>
                {/if}
        {/block}

        {* ESD product badge *}
        {block name='frontend_listing_box_article_esd'}
            {if $sArticle.esd}
                <div class="product--badge badge--esd">
                    <i class="icon--download"></i>
                </div>
            {/if}
        {/block}
                    
    </div>
{/block}






