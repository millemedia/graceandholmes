{block name="widget_emotion_component_html_panel"}
    
    <div class="emotion--html{if !$Data.needsNoStyling} panel{/if}">

        {block name="widget_emotion_component_html_title"}
            
            {if $Data.cms_title}
                <div class="html--title{if !$Data.needsNoStyling}{/if}">
                    {$Data.cms_title}
                </div>
            {/if}
            
        {/block}

        {block name="widget_emotion_component_html_content"}
            
            <div class="html--content{if !$Data.needsNoStyling && $Data.cms_title} panel--body is--wide{/if}">
                {$Data.text}
            </div>
            
        {/block}
        
    </div>
        
{/block}