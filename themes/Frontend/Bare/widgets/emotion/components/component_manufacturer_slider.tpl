{block name="frontend_widgets_manufacturer_slider"}
    
    <div class="emotion--manufacturer panel">

        {* Manufacturer title *}
        {block name="frontend_widgets_manufacturer_slider_title"}
            
            {if $Data.manufacturer_slider_title}
                <div class="manufacturer--title">
                    {$Data.manufacturer_slider_title}
                </div>
            {/if}
            
        {/block}

        {* Manufacturer Content *}
        {block name="frontend_widgets_manufacturer_slider_content"}
            
            <div class="manufacturer--content">

                {block name="frontend_widgets_manufacturer_slider_container"}
                    
                    <div class="manufacturer--slider product-slider"
                         data-product-slider="true"
                         data-itemMinWidth="280"
                         data-arrowControls="{if $Data.manufacturer_slider_arrows == 1}true{else}false{/if}"
                         data-autoSlide="{if $Data.manufacturer_slider_rotation == 1}true{else}false{/if}"
                         {if $Data.manufacturer_slider_scrollspeed}data-animationSpeed="{$Data.manufacturer_slider_scrollspeed}"{/if}
                         {if $Data.manufacturer_slider_rotatespeed}data-autoSlideSpeed="{$Data.manufacturer_slider_rotatespeed / 1000}"{/if}>

                        <div class="product-slider--container">
                            
                            {foreach $Data.values as $supplier}
                                
                                {block name="frontend_widgets_manufacturer_slider_item"}
                                {if $supplier.image}
                                    <div class="manufacturer--item product-slider--item">

                                        {block name="frontend_widgets_manufacturer_slider_item_link"}
                                            <a href="{$supplier.link}" title="{$supplier.name|escape}" class="manufacturer--link">
                                                
                                                {block name="frontend_widgets_manufacturer_slider_item_image"}
                                                    <img class="manufacturer--image" src="{$supplier.image}" alt="{$supplier.name|escape}" />
                                                {/block}
                                                
                                            </a>
                                        {/block}
                                        
                                    </div>
                                {/if}
                                {/block}
                                
                            {/foreach}
                            
                        </div>
                        
                    </div>
                        
                {/block}
                
            </div>
                
        {/block}
        
    </div>
        
{/block}
